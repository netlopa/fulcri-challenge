import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FulcriSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';

@NgModule({
  imports: [FulcriSharedModule, RouterModule.forChild([HOME_ROUTE])],
  declarations: [HomeComponent]
})
export class FulcriHomeModule {}
