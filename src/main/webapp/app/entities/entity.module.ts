import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'song',
        loadChildren: () => import('./song/song.module').then(m => m.FulcriSongModule)
      },
      {
        path: 'artist',
        loadChildren: () => import('./artist/artist.module').then(m => m.FulcriArtistModule)
      },
      {
        path: 'album',
        loadChildren: () => import('./album/album.module').then(m => m.FulcriAlbumModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class FulcriEntityModule {}
