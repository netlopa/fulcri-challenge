import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { ISong, Song } from 'app/shared/model/song.model';
import { SongService } from './song.service';
import { IArtist } from 'app/shared/model/artist.model';
import { ArtistService } from 'app/entities/artist/artist.service';
import { IAlbum } from 'app/shared/model/album.model';
import { AlbumService } from 'app/entities/album/album.service';

type SelectableEntity = IArtist | IAlbum;

@Component({
  selector: 'jhi-song-update',
  templateUrl: './song-update.component.html'
})
export class SongUpdateComponent implements OnInit {
  isSaving = false;
  artists: IArtist[] = [];
  albums: IAlbum[] = [];

  editForm = this.fb.group({
    id: [],
    title: [null, [Validators.required]],
    duration: [null, [Validators.required, Validators.min(0), Validators.max(1000)]],
    year: [null, [Validators.required, Validators.min(1900), Validators.max(2021)]],
    language: [null, [Validators.required]],
    artists: [],
    albums: []
  });

  constructor(
    protected songService: SongService,
    protected artistService: ArtistService,
    protected albumService: AlbumService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ song }) => {
      this.updateForm(song);

      this.artistService.query().subscribe((res: HttpResponse<IArtist[]>) => (this.artists = res.body || []));

      this.albumService.query().subscribe((res: HttpResponse<IAlbum[]>) => (this.albums = res.body || []));
    });

    this.activatedRoute.queryParams.subscribe(params => {
      if (params.context === 'returnToNewSong') {
        const songJson = localStorage.getItem('new-song-wip');
        if (songJson !== '' && songJson != null) {
          const song = JSON.parse(songJson);
          if (song != null) this.updateForm(song);
        }
        localStorage.removeItem('new-song-wip');
      }
    });
  }

  updateForm(song: ISong): void {
    this.editForm.patchValue({
      id: song.id,
      title: song.title,
      duration: song.duration,
      year: song.year,
      language: song.language,
      artists: song.artists,
      albums: song.albums
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const song = this.createFromForm();
    if (song.id !== undefined) {
      this.subscribeToSaveResponse(this.songService.update(song));
    } else {
      this.subscribeToSaveResponse(this.songService.create(song));
    }
  }

  private createFromForm(): ISong {
    return {
      ...new Song(),
      id: this.editForm.get(['id'])!.value,
      title: this.editForm.get(['title'])!.value,
      duration: this.editForm.get(['duration'])!.value,
      year: this.editForm.get(['year'])!.value,
      language: this.editForm.get(['language'])!.value,
      artists: this.editForm.get(['artists'])!.value,
      albums: this.editForm.get(['albums'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ISong>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: SelectableEntity): any {
    return item.id;
  }

  getSelected(selectedVals: SelectableEntity[], option: SelectableEntity): SelectableEntity {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }

  createNewArtist(): void {
    localStorage.setItem('new-song-wip', JSON.stringify(this.createFromForm()));
    this.router.navigate(['/artist/new'], { queryParams: { context: 'fromNewSong' } });
  }

  createNewAlbum(): void {
    localStorage.setItem('new-song-wip', JSON.stringify(this.createFromForm()));
    this.router.navigate(['/album/new'], { queryParams: { context: 'fromNewSong' } });
  }
}
