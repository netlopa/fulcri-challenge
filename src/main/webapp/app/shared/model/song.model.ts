import { IArtist } from 'app/shared/model/artist.model';
import { IAlbum } from 'app/shared/model/album.model';
import { Language } from 'app/shared/model/enumerations/language.model';

export interface ISong {
  id?: number;
  title?: string;
  duration?: number;
  year?: number;
  language?: Language;
  artists?: IArtist[];
  albums?: IAlbum[];
}

export class Song implements ISong {
  constructor(
    public id?: number,
    public title?: string,
    public duration?: number,
    public year?: number,
    public language?: Language,
    public artists?: IArtist[],
    public albums?: IAlbum[]
  ) {}
}
