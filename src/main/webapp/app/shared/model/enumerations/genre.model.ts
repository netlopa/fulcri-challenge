export const enum Genre {
  ROCK = 'ROCK',
  POP = 'POP',
  METAL = 'METAL',
  HOUSE = 'HOUSE',
  COUNTRY = 'COUNTRY'
}
