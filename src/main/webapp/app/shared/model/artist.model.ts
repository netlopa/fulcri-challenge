import { ISong } from 'app/shared/model/song.model';
import { Language } from 'app/shared/model/enumerations/language.model';
import { Genre } from 'app/shared/model/enumerations/genre.model';

export interface IArtist {
  id?: number;
  name?: string;
  language?: Language;
  genre?: Genre;
  songs?: ISong[];
}

export class Artist implements IArtist {
  constructor(public id?: number, public name?: string, public language?: Language, public genre?: Genre, public songs?: ISong[]) {}
}
