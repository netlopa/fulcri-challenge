package com.netlopa.fulcri.repository;

import com.netlopa.fulcri.domain.Song;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Song entity.
 */
@Repository
public interface SongRepository extends JpaRepository<Song, Long> {

    @Query(value = "select distinct song from Song song left join fetch song.artists left join fetch song.albums",
        countQuery = "select count(distinct song) from Song song")
    Page<Song> findAllWithEagerRelationships(Pageable pageable);

    @Query("select distinct song from Song song left join fetch song.artists left join fetch song.albums")
    List<Song> findAllWithEagerRelationships();

    @Query("select song from Song song left join fetch song.artists left join fetch song.albums where song.id =:id")
    Optional<Song> findOneWithEagerRelationships(@Param("id") Long id);
}
