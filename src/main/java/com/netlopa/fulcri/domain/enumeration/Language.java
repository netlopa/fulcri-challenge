package com.netlopa.fulcri.domain.enumeration;

/**
 * The Language enumeration.
 */
public enum Language {
    FRENCH, ENGLISH, SPANISH, ITALIAN
}
