package com.netlopa.fulcri.domain.enumeration;

/**
 * The Genre enumeration.
 */
public enum Genre {
    ROCK, POP, METAL, HOUSE, COUNTRY
}
