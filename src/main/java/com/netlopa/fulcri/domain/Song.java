package com.netlopa.fulcri.domain;


import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

import com.netlopa.fulcri.domain.enumeration.Language;

/**
 * A Song.
 */
@Entity
@Table(name = "song")
public class Song implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @NotNull
    @Min(value = 0L)
    @Max(value = 1000L)
    @Column(name = "duration", nullable = false)
    private Long duration;

    @NotNull
    @Min(value = 1900L)
    @Max(value = 2021L)
    @Column(name = "year", nullable = false)
    private Long year;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "language", nullable = false)
    private Language language;

    @ManyToMany
    @JoinTable(name = "song_artist",
               joinColumns = @JoinColumn(name = "song_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "artist_id", referencedColumnName = "id"))
    private Set<Artist> artists = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "song_album",
               joinColumns = @JoinColumn(name = "song_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "album_id", referencedColumnName = "id"))
    private Set<Album> albums = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public Song title(String title) {
        this.title = title;
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getDuration() {
        return duration;
    }

    public Song duration(Long duration) {
        this.duration = duration;
        return this;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Long getYear() {
        return year;
    }

    public Song year(Long year) {
        this.year = year;
        return this;
    }

    public void setYear(Long year) {
        this.year = year;
    }

    public Language getLanguage() {
        return language;
    }

    public Song language(Language language) {
        this.language = language;
        return this;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Set<Artist> getArtists() {
        return artists;
    }

    public Song artists(Set<Artist> artists) {
        this.artists = artists;
        return this;
    }

    public Song addArtist(Artist artist) {
        this.artists.add(artist);
        artist.getSongs().add(this);
        return this;
    }

    public Song removeArtist(Artist artist) {
        this.artists.remove(artist);
        artist.getSongs().remove(this);
        return this;
    }

    public void setArtists(Set<Artist> artists) {
        this.artists = artists;
    }

    public Set<Album> getAlbums() {
        return albums;
    }

    public Song albums(Set<Album> albums) {
        this.albums = albums;
        return this;
    }

    public Song addAlbum(Album album) {
        this.albums.add(album);
        album.getSongs().add(this);
        return this;
    }

    public Song removeAlbum(Album album) {
        this.albums.remove(album);
        album.getSongs().remove(this);
        return this;
    }

    public void setAlbums(Set<Album> albums) {
        this.albums = albums;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Song)) {
            return false;
        }
        return id != null && id.equals(((Song) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Song{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", duration=" + getDuration() +
            ", year=" + getYear() +
            ", language='" + getLanguage() + "'" +
            "}";
    }
}
