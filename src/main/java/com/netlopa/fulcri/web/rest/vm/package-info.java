/**
 * View Models used by Spring MVC REST controllers.
 */
package com.netlopa.fulcri.web.rest.vm;
